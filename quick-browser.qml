import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import QtWebKit 3.0

ApplicationWindow {
  id: window
  title: "Quick Browser"

  minimumWidth: 256
  minimumHeight: 256

  // We should save dimensions in localstorage.
  width: Screen.width / 2
  height: Screen.height / 2

  property WebView currentPage

  menuBar: MenuBar {
    id: menuBar

    Menu {
      id: fileMenu
      title: qsTr("&File")
      MenuItem {
        text: qsTr("&Open")
        iconName: "document-open"
        shortcut: StandardKey.Open
        onTriggered: openURLs.open()
      }

      MenuItem {
        text: qsTr("&Exit")
        iconName: "application-exit"
        shortcut: StandardKey.Quit
        onTriggered: Qt.quit();
      }
    }

    Menu {
      id: editMenu
      title: "&Edit"
      MenuItem { action: cutAction }
      MenuItem { action: copyAction }
      MenuItem { action: pasteAction }
    }

    Menu {
      id: helpMenu
      title: qsTr("&Help")
      MenuItem {
        text: qsTr("&About")
        iconName: "help-about"
        onTriggered: about.open()
      }
    }
  }

  toolBar: ToolBar {
    id: mainBar
    anchors.top: menuBar.top

    RowLayout {
      anchors.fill: parent

      ToolButton {
        id: backButton
        action: backAction
      }

      ToolButton {
        id: forwardButton
        action: forwardAction
      }

      TextField {
        id: urlField
        placeholderText: "Enter a URL..."
        Layout.fillWidth: true

        Rectangle {
          height: parent.height
          width: currentPage.loading ? parent.width * currentPage.loadProgress / 100 : 0
          anchors.left: parent.left
          color: "#1A3A5FFF"
        }
      }

      ToolButton {
        id: goButton
        action: goURL
      }
    }
  }

  TabView {
    id: tabArea
    anchors.fill: parent
    tabsVisible: count > 1

    style: TabViewStyle {
      tabsMovable: true
      frame: Rectangle { anchors.fill: parent; color: palette.window }
      tab: Label {
        width: window.width / 10
        text: styleData.title
        elide: Text.elideRight
      }

    }

    Component.onCompleted: {
      // TODO: Restore session or homgepage if set when we implement state.
      loadURL("https://www.google.com")
      //urlField.text = getTab(currentIndex).item.url
    }

    onCurrentIndexChanged: {
      updateTabs()
    }

    function updateTabs() {
      for(var index = 0; index < count; index++) {
        var tab = getTab(index)
        if(index == currentIndex) {
          urlField.text = tab.item.url
          currentPage = tab.item
        }
        if(tab.item.url && !tab.item.loading) tab.title = tab.item.title
      }
    }
  }

  statusBar: StatusBar {
    // We have a status bar, but for now its disabled. We need a use for it!
    id: statusBar
    visible: false
  }

  FileDialog {
    id: openURLs
    title: "Select a Web Document"
    nameFilters: ["Web Pages(*.html *.htm)"]
    selectMultiple: true
    onAccepted: openTabs(fileUrls)
  }

  MessageDialog {
    id: about
    title: "About"
    text: qsTr("Matthew Scheirer (C) 2014.
An attempt to explore the power of QtQuick 2.2, by writing a comprehensive web browser in minimal lines of code.")
    informativeText: qsTr("Licensed under the GNU GPL3 or later.")
    detailedText: qsTr("This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public \
License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY \
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.")
    standardButtons: StandardButton.Close
  }

  Action {
    id: copyAction
    text: "&Copy"
    shortcut: "ctrl+c"
    iconName: "edit-copy"
    enabled: (!!activeFocusItem && !!activeFocusItem["copy"])
    onTriggered: activeFocusItem.copy()
  }

  Action {
    id: cutAction
    text: "Cu&t"
    shortcut: "ctrl+x"
    iconName: "edit-cut"
    enabled: (!!activeFocusItem && !!activeFocusItem["cut"])
    onTriggered: activeFocusItem.cut()
  }

  Action {
    id: pasteAction
    text: "&Paste"
    shortcut: "ctrl+v"
    iconName: "edit-paste"
    enabled: (!!activeFocusItem && !!activeFocusItem["paste"])
    onTriggered: activeFocusItem.paste()
  }

  Action {
    id: backAction
    iconName: "go-previous"
    onTriggered: tabArea.getTab(tabArea.currentIndex).item.goBack()
  }

  Action {
    id: forwardAction
    iconName: "go-next"
    onTriggered: tabArea.getTab(tabArea.currentIndex).item.goForward()
  }

  Action {
    id: goURL
    iconName: "arrow-right"
    shortcut: "return"
    onTriggered: {
      // We need to sanitize the URL here
      currentPage.url = urlField.text
    }
  }

  Action {
    id: newTab
    shortcut: "Ctrl+T"
    onTriggered: tabArea.addTab("New Tab", webPage)
  }

  Action {
    id: openLocalTab

  }

  SystemPalette {
    id: palette
  }

  Component {
    id: webPage

    //Item {
      //ToolBar {}
      WebView {
        id: webView
        url: "https://google.com"
        onTitleChanged: tabArea.updateTabs()
      }
   // }
  }

  function loadURL(url) {
    var appendTab = tabArea.addTab("Loading...", webPage)
    appendTab.item.url = url
  }

  function loadURLs(urls) {
    for(var index in urls) loadURL(urls[index])
  }
}
